#For making HTTP requests
import requests
#Lets you use and work with JSON data
import json
#Imports both API keys used later for authentication
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


#Function retrieves photo associated with city-state
#Take city and state arguments
def get_city_photo(city, state):

  #The source of the photos (search endpoint)
  url = "https://api.pexels.com/v1/search"
  #Custom Headers: https://requests.readthedocs.io/en/latest/user/quickstart/#custom-headers
	#Key is 'Authorization' and the Value is the API key
  headers = {
    "Authorization": PEXELS_API_KEY,
  }

  #https://requests.readthedocs.io/en/latest/user/quickstart/#passing-parameters-in-urls
  payload = {
		#Combines city-state parameters to make a search query
    "query": f'{city} {state}',
    #API reserved keyword: "per_page"
		#Specify # of photos per page
    "per_page": 1,
  }

  #Retreive image from 'url'
	#Based on the given parameter('payload'), and headers('Authorization' key)
  response = requests.get(url, params=payload, headers=headers)

	#.content is part of the response returned from request.get()
	#Takes Python value and converts it to a JSON string
  #Could also set to response.json()
  photo_dict = json.loads(response.content)

	#Attempts to access the first photo's URL in photo_dict
  try:
    #https://www.pexels.com/api/documentation/#photos-curated
    #Pull 'url' from nested list-dictionary
    return photo_dict["photos"][0]["url"]
	#KeyError - if the expected key doesn't exist
	#IndexError - list index is out of range
  except (KeyError, IndexError):
    return None



#Retreives weather data for city-state
#Takes two arguments : city, state
def get_city_weather(city, state):

  #Combines city-state parameters to make a search query
  payload = f'{city},{state},US'

  #Sets 'api_key' to the weather API key
  api_key = OPEN_WEATHER_API_KEY

  #Uses city-state('payload') info and API key to retrieve the coordinates
  geocode_url = f'http://api.openweathermap.org/geo/1.0/direct?q={payload}&appid={api_key}'

  #QUESTION: Sends an HTTP GET request to geocode_url
  geocode_response = requests.get(geocode_url)

	#.json() method used
  #Converts JSON content into a Python dictionary
  geocode_data = geocode_response.json()

  #Pulls 'lat' and 'lon' from 'geocode_data' nested dictionary-list
  lat = geocode_data[0]['lat']
  lon = geocode_data[0]['lon']

  #Set lat={lat} and lon={lon}
	#Using this URL containing the coordinates we can pull the weather data
  weather_url = f'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={api_key}'

  #Retreive data using HTTP GET request
  weather_response = requests.get(weather_url)

  #.json converts the response into a dictionary
  weather_data = weather_response.json()

  #Pull 'temp' and 'description' key's values from nested dictionary-list
  temp = weather_data["main"]["temp"]
  description = weather_data["weather"][0]["description"]

  #New dictionary containing both temp and description in one place
  weather_dict = {
    "temp": temp,
    "description": description,
  }

  #return dictionary
  return weather_dict
